import React, {useContext} from "react"

import {Context} from '../Context'
import CartItem from '../components/CartItem'

const Cart = () => {
    const {cartItems, clearCartItems, submit} = useContext(Context)
    const cartElements = cartItems.map(cartItem => (
        <CartItem key={cartItem.id} item={cartItem} />
    ))

    const totalCost = `${(5.99 * cartItems.length).toLocaleString("en-US", {style: "currency", currency: "USD"})}`

    const displayOrderButton = () => {
        if(cartItems.length > 0) {
            return <button onClick={() => clearCartItems()}>{!submit ? 'Place Order': 'Ordering...'}</button>
        }
    } 

    return (
        <main className="cart-page">
            <h1>Check out</h1>
            {cartElements}
    <p className="total-cost">Total: {totalCost}</p>
            <div className="order-button">
                {displayOrderButton()}
            </div>
        </main>
    )
}

export default Cart