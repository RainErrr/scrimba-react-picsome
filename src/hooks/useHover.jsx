import {useState, useEffect, useRef} from 'react'

const useHover = () => {
    const [hovered, setHovered] = useState(false)
    const inputRef = useRef(null)

    const enter = () => {
        setHovered(true)
    }

    const leave = () => {
        setHovered(false)
    }

    useEffect(() => {
        inputRef.current.addEventListener('mouseenter', enter)
        inputRef.current.addEventListener('mouseleave', leave)

        return () => {
            inputRef.current.removeEventListener('mouseenter', enter)
            inputRef.current.removeEventListener('mouseleave', leave)
        }
    }, [])


    return [hovered, inputRef]
}

export default useHover
