import React, { useState, useEffect } from 'react'

const Context = React.createContext()

const ContextProvider = ({ children }) => {
    const [allPhotos, setAllPhotos] = useState([])
    const [cartItems, setCartItems] = useState([])
    const [submit, setSubmit] = useState(false)

	const url =
		'https://raw.githubusercontent.com/bobziroll/scrimba-react-bootcamp-images/master/images.json'
	useEffect(() => {
		fetch(url)
			.then((response) => response.json())
			.then((data) => setAllPhotos(data))
	}, [])

	const toggleFavorite = (id) => {
		const updatePhoto = allPhotos.map((photo) => {
			if (photo.id === id) {
				return { ...photo, isFavorite: !photo.isFavorite }
			}
			return photo
		})
		setAllPhotos(updatePhoto)
    }
    
    const addToCart = (newItem) => {
        setCartItems(prevItems => [...prevItems, newItem])
    }

    const removeFromCart = (id) => {
        const findByIndex = cartItems.findIndex(item => item.id === id)

        setCartItems(prevItems => [
            ...prevItems.slice(0, findByIndex), 
            ...prevItems.slice(findByIndex + 1)
        ]) // can be done with filter, but this is good for practise
    }

    const clearCartItems = () => {
        setSubmit(true)
        
        setTimeout(() => {
            setCartItems([])
            console.log('Order sent!');
            setSubmit(false)
        }, 3000)
    }

    console.log(cartItems);
    
	return (
        <Context.Provider value={{ 
            allPhotos, 
            cartItems, 
            submit, 
            toggleFavorite, 
            addToCart, 
            removeFromCart, 
            clearCartItems 
        }}>
			{children}
		</Context.Provider>
	)
}

export { ContextProvider, Context }
